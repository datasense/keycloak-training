# Overview of OAuth2

## Technical requirements

| Software required                   | OS required                        |
| ------------------------------------| -----------------------------------|
| Keycloak 18.0.0                     | Windows, Mac OS or Linux ...       |
| Node.js 14+                         | Windows, Mac OS or Linux ...       |

download apps/app-3 and change to app-3

```
apps/app-3(main -> origin)
λ ll
total 8
drwxr-xr-x 1 hiche 197609 0 May 20 01:28 ./
drwxr-xr-x 1 hiche 197609 0 May 20 01:28 ../
drwxr-xr-x 1 hiche 197609 0 May 21 13:07 backend/
drwxr-xr-x 1 hiche 197609 0 May 21 13:06 frontend/
```
## Description of the Application

The application consists of an overview of OAuth2. The purpose of this application is to help you dicover/understand the following topics:

* Obtaining an access token
* Requiring user consent
* Limiting the access granted to access tokens
* Validating access tokens

## Running the Application

* to run the application, open a terminal and run the follownig commands:

Change to **frontend** folder
```
/apps/app-3/frontend(main -> origin) (app-3-frontend@0.0.1)
λ npm install

/apps/app-3/frontend(main -> origin) (app-3-frontend@0.0.1)
λ npm start
```

Change to **backend** folder
```
/apps/app-3/backend(main -> origin) (app-3-backend@0.0.1)
λ npm install

/apps/app-3/backend(main -> origin) (app-3-backend@0.0.1)
λ npm start
```

Open http://localhost:8000/ in your browser

## Prerequisites for using the application:

* Keycloak up and running (http://localhost:8080)
* A realm named **myrealm** (created in lab-1)
* A client with:
    * Client ID: oauth2-client
    * Access Type: public
    * Valid Redirect URIs: http://localhost:8000/
    * Web Origins: http://localhost:8000
* A user that you can log in with
    * the user must have the global realm role: **securedEndpoint**

## Obtaining an Access Token

* Click on **Authorization**
* Click Send **Authorization Request**
    * check the Access Token claims

## Requiring user consent

1. Consent on scopes the application is requesting
    * Select **oauth2-client** from Clients list
    * Switch **ON** **Consent Required** attribute
    * Click on **Authorization**

Go back to the Application: 
* Click Send **Authorization Request**
    * You should be prompted to grant access to the application

2. Consent for a new required client scope
    * Click on **Client Scopes** 
    * Click on **Create**
    * Fill in the form with following:
        * Name: **folders**
        * Display On Consent Screen: **ON**
        * Consent Screen Text: Provide Access To Your Folders
    * Click Save
    * Go back to **oauth2-client** and Select **Client Scopes**
    * Select **folders** in **Optional Client Scopes** and Click **Add Selected**

Go back to the Application: 
* Click on **Authorization**
* Click Send **Authorization Request**

3. Go to **Account Console**: http://localhost:8080/realms/myrealm/account and login with your user
* Under Applications you can see permissions you've granted to **oauth2-client**

## Limiting the access granted to access tokens

It is important to limit the access granted. Otherwise, any access token could potentially be used to access any resource the user has access to.

We'll explore three different ways to limit access for a specific access token:

* Audience: Allows listing the resource providers that should accept an
access token
* Roles: Through controlling what roles a client has access to, it is possible to control what roles an application can access on behalf of the user
* Scope: In Keycloak, scopes are created through client scopes, and an application can only have access to a specific list of scopes

### 1. **Using the audience to limit token access**

Until now, the issued access tokens to the frontend application do not include backend application in the audience.
The backend has not been configured to check the audience in the token, that why it has been working...

* Configuring the backend:
    * Open /backend/keycloak.json file in a text editor. Change the value of the **verify-token-audience** field to **true**, as shown in the following:
    ```
        {
        "realm": "myrealm",
        "bearer-only": true,
        "auth-server-url": "${env.KC_URL:http://localhost:8080/}",
        "resource": "oauth2-backend",
        "verify-token-audience": true
        }
    ```
    * If you try now to invoke the secured endpoint you will have a **403: Access Denied** error

* Create a new client with the Client ID value set as **oauth2-backend**
* Change Access Type of the created Client to **bearer-only**, then Click Save
* Navigate to **oauth2-client**, Click on Mappers, then Click on Create and fill in the form with following:
    * Name: **oauth2 backend audience**
    * Mapper Type: **Audience**
    * Included Client Audience: **oauth2-backend**
* Click Save
* The call to the backend service should succceed now

### 2. **Using roles to limit token access**

The roles included in tokens are the intersection between the roles a user has and the roles a client is allowed to use.

By default, all roles for a given user are included in the token. This is for convenience when getting started with Keycloak and you should not include all roles in a production scenario.

#### **Adding role through Scope**

* Navigate to **oauth2-client**
* Click on **Scope** tab
* Swith OFF **Full Scope Allowed** option

Now the access token contains no roles and thue **aud** claim includes only **oauth2-backend**

* Under **Scope** tab of **oauth2-client**, Select in **Realm Roles **: **securedEndpoint**
* Click **Add Selected**

Go back to the application and get a new Access Token, it will contain:
```
"realm_access": {
    "roles": [
    "securedEndpoint"
    ]
}
```
which will make service invocation successfull.

#### **Adding role through Client Scope**
First remove the scope that the **oauth2-client** has on the **securedEndpoint** role.
We are back to the situation where the access token does not contain any role.

* From the Admin Console, Create a Client Scope: **secured-endpoint-client-scope**
* From the scope tab add **securedEndpoint** in Realm Roles section
* Add this created Client Scope to **oauth2-client** as Optional Client Scopes

Back to tha application: Request Access Token by setting scope to: **secured-endpoint-client-scope**

### 3. **Using the scope to limit token access**
The default mechanism in OAuth 2.0 to limit the permissions for an access token is through scopes directly.
Within Keycloak, a scope in OAuth 2.0 is mapped to a client scope.
We will simply define empty client scopes that have no protocol mappers and do not have access to any roles.

* From Client Scopes left-hand menu, Create:
    * folders:view
    * folders:create
    * folders:delete
    * Enter some value for **Consent Screen Text** field
* From **oauth2-client** Client Scope tab add: **folders:view** as Default Client Scope and add **folders:create** and **folders:delete** as Optional Client Scopes
* Request new access token with desired scopes

## Validating the Access Token

You can validate an access token, either :
* by invoking the token introspection endpoint provided by Keycloak
* or by directly verifying the access token

### Invoking the /instrospect endpoint:

* From **oauth2-backend** client, CLick on credentials
* Copy the value of **Secret** field
* set the secret to an environment variable:

        export secret=*********

* From the application, request a new Access Token and Copy the encoded access token at the bottom of the page
* set the token to an environment variable:

        export token=*********


```
curl --data "client_id=oauth2-backend&client_secret=%secret%&token=%token%" http://localhost:8080/realms/myrealm/protocol/openid-connect/token/introspect | jq
```

you will get a resonse as the following:

```
{
  "exp": 1653179839,
  "iat": 1653179539,
  "auth_time": 1653178085,
  "jti": "51a41d6e-0386-4426-a6c9-1d476f9ee7b2",
  "iss": "http://localhost:8080/realms/myrealm",
  "aud": "oauth2-backend",
  "sub": "a95c4c81-fd21-4a5e-9be5-7e0d1faf8ec3",
  "typ": "Bearer",
  "azp": "oauth2-client",
  "session_state": "b59b6ea8-3130-46e4-aeea-4780493ab871",
  "name": "Hichem Kaighani",
  "given_name": "Hichem",
  "family_name": "Kaighani",
  "preferred_username": "hkaighani",
  "picture": "http://datasense-group.fr:88/wp-content/uploads/2022/02/logo_data_06.png",
  "email": "hichem.kaighani@datasense-group.com",
  "email_verified": false,
  "acr": "0",
  "allowed-origins": [
    "http://localhost:8000"
  ],
  "scope": "folders:view folders:delete profile email folders:create",
  "sid": "b59b6ea8-3130-46e4-aeea-4780493ab871",
  "client_id": "oauth2-client",
  "username": "hkaighani",
  "active": true
}
```

in case the token was not valid it would have been:
```
{
  "active": false
}
```

### Directly validating the access token
The other approach to verifying access tokens issued by Keycloak is validating them directly in the application. As Keycloak uses JWT as its access token format, this means you can parse and read the contents directly from your application, as well as verifying that the token was issued by Keycloak as it is signed by Keycloak sing its private signing keys.

All Keycloak client libraries, which are referred to as application adapters by Keycloak, verify tokens directly without the token introspection endpoint.