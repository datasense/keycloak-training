# Overview of OpenID Connect

## Technical requirements

| Software required                   | OS required                        |
| ------------------------------------| -----------------------------------|
| Keycloak 18.0.0                     | Windows, Mac OS or Linux ...       |
| Node.js 14+                         | Windows, Mac OS or Linux ...       |

download apps/app-2 and change to app-2

```
apps/app-2(main -> origin)
λ ls -l
total 48
-rw-r--r-- 1 hiche 197609   468 May 15 17:27 app.js
-rw-r--r-- 1 hiche 197609  9000 May 14 01:28 client.js
-rw-r--r-- 1 hiche 197609   157 May 14 01:28 Dockerfile
-rw-r--r-- 1 hiche 197609  3305 May 17 23:41 index.html
drwxr-xr-x 1 hiche 197609     0 May 15 17:28 node_modules/
-rw-r--r-- 1 hiche 197609   234 May 14 01:28 package.json
-rw-r--r-- 1 hiche 197609 18572 May 15 17:28 package-lock.json
-rw-r--r-- 1 hiche 197609   268 May 14 01:28 styles.css
```
## Description of the Application

The application consists of an overview of OpenID Connect. The purpose of this application is to help you dicover/understand the following topics:

* Discovery endpoint
* User auhtentication
* ID token
* UserInfo endpoint
* logout 

## Running the Application

* to run the application, open a terminal and run the follownig commands:
```
/apps/app-2(main -> origin) (app-2-openid-connect@0.0.1)
λ npm install
npm WARN app-2-openid-connect@0.0.1 No description
npm WARN app-2-openid-connect@0.0.1 No repository field.
npm WARN app-2-openid-connect@0.0.1 No license field.

audited 61 packages in 0.873s

8 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities


/apps/app-2(main -> origin) (app-2-openid-connect@0.0.1)
λ npm start

> app-2-openid-connect@0.0.1 start /apps/app-2
> node app.js
```
Open http://localhost:8000/ in your browser

## Prerequisites for using the application:

* Keycloak up and running (http://localhost:8080)
* A realm named **myrealm** (created in lab-1)
* A client with:
    * Client ID: oidc-client
    * Access Type: public
    * Valid Redirect URIs: http://localhost:8000/
    * Web Origins: http://localhost:8000
* A user that you can log in with

## Discovery endpoint

It is an optional specification that most OpenID Providers implement including **Keycloak**.
A Relying Party can discover can discover OpenID Provider Metadata from a standard endpoint, namely: http://localhost:8080/realms/myrealm/.well-known/openid-configuration.

## User auhtentication

OpenID Connect authorization code flow is the most common way to authenticate a user.
* Discover authorization code flow by going through the application steps
  * Authentication:
    * fill in the form:
      - client_id=oidc-client
      - scope=openid (we'll be doing openid request)
      - prompt=login (require the user to log in again)
      - max_age=60 (Keycloak will re-authenticate the user after 60s)
      - login_hint=[login-of-your-user] (this username will be filled automatically on keycloak login page)
    * Generate Authentication Request
    * Send Authentication Request

## ID Token

  * Token
    * Click Send Token Request
  * Refresh Token
    * Click Send Referesh Request
    * Update some user profile information (email, firstname or lastname) and Click Send Referesh Request, you will notice that the user profile was updated

### Adding a custom property 
1. On the Keycloak admin console go on your user and select **Attributes** tab
2. Add *my-custom-attribute* with some value
3. Click on **Client Scopes** in the left menu, then Click on Create. Enter *my-scope* for the name  and CLick on **Save**
4. Click on Mappers, then click on Create. Fill in the form with the following values:

  - Name: my-user-attribute-mapper
  - Mapper Type: User Attribute
  - User Attribute: my-custom-attribute
  - Token Claim Name: user-attribute-claim
  - Claim JSON Type: String

    Make sure Add to ID Token is turned on, then click on Save
5. click on **Clients** in the left-hand side menu and select oidc-client. Select **Client Scopes** then, in the **Optional Client Scopes** window, select *my-scope* and click on Add
selected.
6. Go back on the application and under **Authentication** section add *my-scope* to the scope field. the client has to explicitly request this scope as it has been added as optional client scope. Click on **Genereate Authentication Request** then Click on **Send Authentication Request**

### Adding Role to ID Token

1. Select **Client Scopes** 
2. Select the roles client scope 
3. Click on Mappers, then select realm roles
4. Turn on Add to ID Token, and Click Save

Now you will see **realm_access** within the ID token

## UserInfo endpoint

you can also configure what information is returned in the UserInfo endpoint.

1. click on **Clients** in the left-hand side menu and select *oidc-client*
2. Click on Mappers, then click on Create. Fill in the following:
  * Name: my-claim-mapper
  * Mapper Type: Hardcoded claim
  * Token Claim Name: my-claim
  * Claim value: my claim value
  * Claim JSON Type: String
  * Add to userinfo: ON

the UserInfo endpoint is that it can only be invoked with an access token obtained through an OIDC flow
