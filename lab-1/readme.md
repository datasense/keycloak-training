# Introduction

## Installing and Running Keycloak

* Runnning as a Docker container
    * prerequisites: need to have Docker intalled (https://www.docker.com/)
        ```
        docker run -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --name my_keycloak -p 8080:8080 quay.io/keycloak/keycloak start-dev
        ```

        ```
        docker run -d -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --name my_keycloak -p 8080:8080 quay.io/keycloak/keycloak start-dev
        ```

* Installing and Running locally
    * prerequisites: JDK 8+ installed (https://adoptium.net/)
    * Download Keycloak distribution (https://github.com/keycloak/keycloak/releases/download/18.0.0/keycloak-18.0.0.zip)
    * Keycloak does not come with a default admin user, so either
        * start keycloak 
            ```
            bin\kc start-dev
            ```
            and fill in the form with your admin's username and password at http://localhost:8080

        * define environment variables: 
            ```
            export KEYCLOAK_ADMIN=admin
            export KEYCLOAK_ADMIN_PASSWORD=admin
            ```
            and start keycloak 
            ```
            bin\kc start-dev
            ```

## Creating a realm

* Create a realm using Keycloak admin console
    * Hover your mouse over the realm selector in the topleft corner (just below the Keycloak logo).
    * Then Click on the Add realm button
    * Enter a name for the realm

* Create a realm using the Admin CLI
    * Start an authenticated session by logging in:
        ```
        kcadm config credentials --server http://localhost:8080/ --realm master --user admin
        Logging into http://localhost:8080/ as user admin of realm master
        Enter password: admin
        ```
    * create {realm-name}:
        ```
        kcadm create realms -s realm={realm-name} -s enabled=true -o
        ```

## Creating a user

* Create a user using Keycloak admin console
    * From the left-hand menu, click on Users, and then click on Add User
    * Enter a username for the user
    * Click on Save

    The user will need an initial password set to be able to login. To do this:

    * Click Credentials (top of the page)
    * Fill in the Set Password form with a password
    * Click Set Password

* Create a user using the Admin CLI
    * Start an authenticated session by logging in:
        ```
        kcadm config credentials --server http://localhost:8080/ --realm master --user admin
        Logging into http://localhost:8080/ as user admin of realm master
        Enter password: admin
        ```
    * create user:
        ```
        kcadm create users -r {realm-name} -s username={user-name} -s enabled=true -o
        kcadm set-password -r {realm-name} --username {user-name} --new-password password
        ```

## Creating a group

* Create a group using Keycloak admin console
    * From the left-hand menu, click on Groups, and then click on New
    * Enter a name for the group
    * Click on Save

* Create a group using  the Admin CLI
    * Start an authenticated session by logging in:
        ```
        kcadm config credentials --server http://localhost:8080/ --realm master --user admin
        Logging into http://localhost:8080/ as user admin of realm master
        Enter password: admin
        ```
    * create group:
        ```
        kcadm create groups -r {realm-name} -s name={group-name} -o
        kcadm update -r {realm-name} users/{user-id}/groups/{group-id}
        ```

## Creating a global role

* Create a global role using Keycloak admin console
    * From the left-hand menu, click on Roles, and then click on Add Role
    * Enter a Role Name 
    * Click on Save

* Create a global role using  the Admin CLI
    * Start an authenticated session by logging in:
        ```
        kcadm config credentials --server http://localhost:8080/ --realm master --user admin
        Logging into http://localhost:8080/ as user admin of realm master
        Enter password: admin
        ```
    * create role:
        ```
        kcadm create roles -r {realm-name} -s name={role-name} -o
        kcadm create -r {realm-name} users/{user-id}/role-mappings/realm -b "[{\"id\":\"{role-id}\",\"name\":\"{role-name}\",\"composite\":false,\"clientRole\":false,\"containerId\":\"{realm-name}\"}]"
        ```
