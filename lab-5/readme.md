# Securing Native Application

## Technical requirements

| Software required                   | OS required                        |
| ------------------------------------| -----------------------------------|
| Keycloak 18.0.0                     | Windows, Mac OS or Linux ...       |
| Node.js 14+                         | Windows, Mac OS or Linux ...       |

download apps/app-4 and change to app-4

```
apps/app-4(main -> origin)
λ ll
total 5
drwxr-xr-x 1 hiche 197609    0 May 23 23:52 ./
drwxr-xr-x 1 hiche 197609    0 May 22 19:06 ../
-rw-r--r-- 1 hiche 197609 1199 May 23 23:31 app.js
-rw-r--r-- 1 hiche 197609  208 May 14 01:28 package.json
```

## Description of the Application

The application is uses the system browser to obtain the authorization code.

## Prerequisites for using the application:

* Keycloak up and running (http://localhost:8080)
* A realm named **myrealm** (created in lab-1)
* A client with:
    * Client ID: native-client
    * Access Type: public
    * Valid Redirect URIs: http://localhost/callback
    * Web Origins: http://localhost:8000

## Running the Application

* to run the application, open a terminal and run the follownig commands:

```
/apps/app-4(main -> origin) (app-4-native@0.0.1)
λ npm install

/apps/app-4(main -> origin) (app-4-native@0.0.1)
λ npm start
```

Result:

```
> app-4-native@0.0.1 start /apps/app-4
> node app.js

Listening on port: 60319
Authorization Code: 4087c6ca-0b7c-4d3f-a6d0-4144e6170497.6b56c7c9-7a55-4895-b124-3f8c05af9858.41382c98-3328-411b-b29e-3255c6bd917b 

Access Token: eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXWU1tTFRWalFULUNoUkNOR1lELWp3VS1fc1RreXZvM2pnUlB2eHdYTWM0In0.eyJleHAiOjE2NTMzNDQ0ODUsImlhdCI6MTY1MzM0NDE4NSwiYXV0aF90aW1lIjoxNjUzMzQ0MTg1LCJqdGkiOiI2YzQyNzhiOS04Y2NmLTQxN2QtYTVhMS0wZWNjMTdi
NTdkNzMiLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgwODAvcmVhbG1zL215cmVhbG0iLCJhdWQiOiJhY2NvdW50Iiwic3ViIjoiYTk1YzRjODEtZmQyMS00YTVlLTliZTUtN2UwZDFmYWY4ZWMzIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibmF0aXZlLWNsaWVudCIsInNlc3Npb25fc3RhdGUiOiI2YjU2YzdjOS03YTU1LTQ4OTUtYjEyNC
0zZjhjMDVhZjk4NTgiLCJhY3IiOiIxIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImRlZmF1bHQtcm9sZXMtbXlyZWFsbSIsIm9mZmxpbmVfYWNjZXNzIiwic2VjdXJlZEVuZHBvaW50IiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFu
YWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6InByb2ZpbGUgZW1haWwiLCJzaWQiOiI2YjU2YzdjOS03YTU1LTQ4OTUtYjEyNC0zZjhjMDVhZjk4NTgiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsIm5hbWUiOiJIaWNoZW0gS2FpZ2hhbmkiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJoa2FpZ2hhbmkiLCJnaX
Zlbl9uYW1lIjoiSGljaGVtIiwiZmFtaWx5X25hbWUiOiJLYWlnaGFuaSIsInBpY3R1cmUiOiJodHRwOi8vZGF0YXNlbnNlLWdyb3VwLmZyOjg4L3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDIyLzAyL2xvZ29fZGF0YV8wNi5wbmciLCJlbWFpbCI6ImhpY2hlbS5rYWlnaGFuaUBkYXRhc2Vuc2UtZ3JvdXAuY29tIn0.XFvvBSzqmvCUpdc2PfH7
aasZ7SGy-snTAey-ziowc4nu3sTLM477U339iLImfxiN8Fr9vYXfD27XXNVOs-07r7px8Z4n7j5EHKIhqNNFfRoYoNWqAwf4xs3UqQfPOg9o_uyInc5_509sM61fJdyjakv72mFUJ42EHHy20qqv4clYggi267_Vx9zLgld0-_waygj60NEYBlSzfgnjaHRr3btyuUR0XWOu1nNvn81deiOD2TFOpPS6dFIDJQtdavM5XKuoJ1KnZb9PJ4Uhhm
sQ837XzgSgQJLV23GCoWpQdp1_O2wTsqSVYqxD6InPoNVkV_-EZCC67jSFa8e9IlKsqw
```

# Securing REST API
Keycloak has support for service accounts, which allows a service to obtain an access token on behalf of itself by using the Client Credential grant type.

* Create a client with:
    * Client ID: **service-account**
    * Client Protocol: **openid-connect**
    * Access Type: **confidential**
    * Standard Flow Enabled: **OFF** (&rarr; The client won't be able to user Authorization Code flow)
    * Implicit Flow Enabled: **OFF**
    * Direct Access Grants Enabled: **OFF**
    * Service Accounts Enabled: **ON** (&rarr; The client can use Client Credentials flow)

* Get an Access Token using Client Credentials Oauth2 grant type: run the following commands:
```
λ set secret=v2Z0NHbuClv7gS4XvruiUerJXYy3kNM8

/apps/app-4(main -> origin) (app-4-native@0.0.1)
λ curl --data "client_id=service-account&client_secret=%secret%&grant_type=client_credentials" http://localhost:8080/realms/myrealm/protocol/openid-connect/token
{"access_token":"eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXWU1tTFRWalFULUNoUkNOR1lELWp3VS1fc1RreXZvM2pnUlB2eHdYTWM0In0.eyJleHAiOjE2NTM0MzAxMzgsImlhdCI6MTY1MzQyOTgzOCwianRpIjoiY2YzZDg1OWQtNzBmZi00YmQ1LWFkYWEtNmVhOGI2ODFiNGYwIiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL3JlYWxtcy9teXJlYWxtIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6ImU4ZTg2ZWE5LTQ0NzYtNDM2ZC04ODJmLTBmYjlmNzhlNGM4MSIsInR5cCI6IkJlYXJlciIsImF6cCI6InNlcnZpY2UtYWNjb3VudCIsImFjciI6IjEiLCJyZWFsbV9hY2Nlc3MiOnsicm9sZXMiOlsiZGVmYXVsdC1yb2xlcy1teXJlYWxtIiwib2ZmbGluZV9hY2Nlc3MiLCJ1bWFfYXV0aG9yaXphdGlvbiJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInNjb3BlIjoicHJvZmlsZSBlbWFpbCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwiY2xpZW50SG9zdCI6IjEyNy4wLjAuMSIsImNsaWVudElkIjoic2VydmljZS1hY2NvdW50IiwicHJlZmVycmVkX3VzZXJuYW1lIjoic2VydmljZS1hY2NvdW50LXNlcnZpY2UtYWNjb3VudCIsImNsaWVudEFkZHJlc3MiOiIxMjcuMC4wLjEifQ.MixZmMvSUl5AfFhPBDRIyX-XvVdJ8blHBDr8kfL-MRHlPFpQ5_eyj7xVyKsdApBI8mDuvKA9-m2dCTWWW-rDS2QxbM_Ty98bB97FD_qFYDaQ7g85o-xfEhqR7JrYDEKcYPfB1oTeMkWflZf6BtZHra3ckefyMGrYPg8oAiZ2lsnTPBPnL1hij2frk6tkquyIPU2sCJPubwVYvBWFZgWX5XyOishdvSvWYgg1s2_TNFBqjVgGr1NWFPlpQ55Y3w4GvwJtejbDp3lUuqN4i1kaqRiodL5OmM-I9SJnnLR1wo0ddJjmS36HgOcvH0ZBQN4znCbO27r7bSk9CEbLoQZ9qw","expires_in":300,"refresh_expires_in":0,"token_type":"Bearer","not-before-policy":1653144753,"scope":"profile email"}
```
