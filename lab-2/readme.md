# Securing an Application

## Technical requirements

| Software required                   | OS required                        |
| ------------------------------------| -----------------------------------|
| Keycloak 18.0.0                     | Windows, Mac OS or Linux ...       |
| Node.js 14+                         | Windows, Mac OS or Linux ...       |

download apps/app-1 and change to app-1

```
apps/app-1(main -> origin)
λ ls -l
total 8
drwxr-xr-x 1 hiche 197609 0 May 14 02:30 backend/
drwxr-xr-x 1 hiche 197609 0 May 14 02:14 frontend/
```

## Description of the Application

The application consists of a javascript frontend SPA web application and backend REST API.
Both frontend and backend are run using Node.js.

* The frontend application:
    * is made available on: http://localhost:8000
    * provides the following features:
        * Login with Keycloak
        * displays the username of connected user
        * displays the user's profile picture
        * displays the Access Token
        * displays the Id token
        * creates new tokens
        * calls the secured endpoints provided by the backend: /secured
    * uses:
        * keycloak.js (povided by keycloak: http://localhost:8080/js/keycloak.js)
        * Express framework (https://expressjs.com/)

* The backend application:
    * is made available on: http://localhost:3000
    * provides two endpoints:
        * /public: A publicly available endpoint
        * /secured: A secured endpoint
    * uses:
        * Keycloak Node.js adapter (https://www.npmjs.com/package/keycloak-connect)
        * Express framework (https://expressjs.com/)

The used libraries fully support OAuth 2.0 or OpenID Connect flows
## How it works?

* The frontend application side:
    * At high level, the frontend authenticates the users using Keycloak, and then invokes the backend, which uses Keycloak to verify that the request should be permitted.
    * The Authentication of the user is completely deledgated to Keycloak, the application does not have to know how to authenticate the user
    * The application is using **authorization code flow in OpenID Connect**
    * the user clicks on the login button &rarr; redirected to the Keycloak login page
    * The user authenticates with Keycloak &rarr; redirected back to the application with an **authorization code** &rarr; The application then invokes Keycloak to exchange the authorization code for:
        * An access token
        * An Id token
        * A refresh token
    * At this point de frontend is ready to call the backend

*  The backend application side: when the frontend invokes the backend
    * The frontend includes the access token within the request to call the backend
        * Keycloak uses JSON Web Signature (JWS): non opaque token
        * The token also includes a digital signature &rarr; the backend can verify that the token was indeed issued by Keycloak without calling Keycloak
            * The backend retrieves Keycloak's public keys (**keys can be cached in memory**).
            * The backend verifies thanks to these keys that the access token was issued by a trusted Keycloak instance and that the token is valid
            * The backend can performs microjurisdiction checks by verifying that the token contains for example the expected role

## Running the Application

* to run the frontend, open a terminal and run the follownig commands:
```
apps/app-1(main -> origin)
λ cd frontend

apps/app-1/frontend(main -> origin) (app-1-frontend@0.0.1)
λ npm install
npm WARN deprecated start@5.1.0: Deprecated in favor of https://github.com/deepsweet/start
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app-1-frontend@0.0.1 No description
npm WARN app-1-frontend@0.0.1 No repository field.
npm WARN app-1-frontend@0.0.1 No license field.

added 61 packages from 47 contributors and audited 61 packages in 2.359s

8 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

apps/app-1/frontend(main -> origin) (app-1-frontend@0.0.1)
λ npm start

> app-1-frontend@0.0.1 start apps/app-1/frontend
> node app.js

Started at port 8000
```
* to run the backend, open a terminal and run the follownig commands:
```
apps/app-1(main -> origin)
λ cd backend

apps/app-1/backend(main -> origin) (app-1-service@0.0.1)
λ npm install

> chromedriver@101.0.0 install apps/app-1/backend/node_modules/chromedriver
> node install.js

ChromeDriver binary exists. Validating...
ChromeDriver is already available at '~/AppData/Local/Temp/101.0.4951.41/chromedriver/chromedriver.exe'.
Copying from ~/AppData/Local/Temp/101.0.4951.41/chromedriver to target path apps/app-1/backend/node_modules/chromedriver/lib/chromedriver
Done. ChromeDriver binary available at apps/app-1/backend/node_modules/chromedriver/lib/chromedriver/chromedriver.exe
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN app-1-service@0.0.1 No description
npm WARN app-1-service@0.0.1 No repository field.
npm WARN app-1-service@0.0.1 No license field.

added 147 packages from 136 contributors and audited 147 packages in 5.232s

17 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

apps/app-1/backend(main -> origin) (app-1-service@0.0.1)
λ npm start

> app-1-service@0.0.1 start /apps/app-1/backend
> node app.js

Started at port 3000
```

## Settings to log in to the Application

* Prerequisites:
    * Keycloak up and running (http://localhost:8080)
    * A realm named **myrealm** (created in lab-1)

* frontend requirements:
    * frontend application registered as: **frontend-app-1** (index.html: var kc = new Keycloak({ realm: 'myrealm', clientId: 'frontend-app-1' });)
    * user created
    * A picture attribute assigned to the user
    * The picture attribute needs to be mapped and added to the access token

* backend requirements:
    * A global role named securedEndpoint
    * The user has to be a member of this global role