# Integrating With Keycloak

## Technical requirements

| Software required                   | OS required                        |
| ------------------------------------| -----------------------------------|
| Keycloak 18.0.0                     | Windows, Mac OS or Linux ...       |
| Node.js 14+                         | Windows, Mac OS or Linux ...       |
| Go                                  | Windows, Mac OS or Linux ...       |

Start Keycloak on port 8180:

```
λ kc start-dev --http-port 8180
```
if using Docker:

```
λ docker run -d -e KEYCLOAK_ADMIN=admin -e KEYCLOAK_ADMIN_PASSWORD=admin --name my_keycloak -p 8180:8080 quay.io/keycloak/keycloak start-dev
```

* Create a client that will be used by browser based apps:
    * Client ID: **browser-app**
    * Root URL: **http://localhost:8080**

* Create a client that will be used by server side web app:
    * Client ID: **server-side-app**
    * Root URL: **http://localhost:8080**
    * Access Type: **Confidential**

* Create a  client that will be used by backend application:
    * Client ID: **backend-app**
    * Root URL: **http://localhost:8080**
    * Access Type: **Confidential**
    * Direct Access Grants Enabled: **ON**

* Create a client that will be used by a reverse proxy:
    * Client ID: **proxy-client**
    * Root URL: **http://localhost**
    * Access Type: **Confidential**

* Create a user in Keycloak:
    * Username : user
    * Password : password

## Integrating with Golang

* Change to app-5/golang
* Change **CLIENT_SECRET** in **main.go** with **server-side-app** client
* Start the application:
```
λ go run main.go
2022/05/26 17:34:09 To authenticate go to http://localhost:8080/
```
* on your browser got to: http://localhost:8080/

The **go-oidc** package provides OpenID Connect capabilities for client applications. But it requires additional work to get it done right, as well as to maintain the code.

## Integrating with Java

Also known as Keycloak adapters, these client-side implementations have support for some of the most common frameworks, web containers, and application servers available.

### Using Quarkus

Quarkus provides an OpenID Connect compliant extension called **quarkus-oidc**. It provides a simple and rich configuration model that can protect both frontend and backend applications.

#### Quarkus web-app application (frontend)

The **web-app** type represents applications that authenticate using Keycloak through the browser, using the authorization code grant type.

* Change to app-5/quarkus/frontend
* Change **CLIENT_SECRET** in the **src/main/resources/application.properties** file with secret of **server-side-app** client
* Start the application:
```
λ mvnw quarkus:dev
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------< org.keycloak:quarkus >------------------------
[INFO] Building quarkus 1.0.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- quarkus-maven-plugin:1.12.1.Final:dev (default-cli) @ quarkus ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] Nothing to compile - all classes are up to date
Listening for transport dt_socket at address: 5005
__  ____  __  _____   ___  __ ____  ______
 --/ __ \/ / / / _ | / _ \/ //_/ / / / __/
 -/ /_/ / /_/ / __ |/ , _/ ,< / /_/ /\ \
--\___\_\____/_/ |_/_/|_/_/|_|\____/___/
2022-05-26 21:00:12,229 WARNING [io.ver.ext.aut.oau.imp.OAuth2AuthProviderImpl] (vert.x-eventloop-thread-2) Skipped unsupported JWK: java.security.NoSuchAlgorithmException: RSA-OAEP
2022-05-26 21:00:12,624 INFO  [io.quarkus] (Quarkus Main Thread) quarkus 1.0.0-SNAPSHOT on JVM (powered by Quarkus 1.12.1.Final) started in 3.920s. Listening on: http://localhost:8080
2022-05-26 21:00:12,630 INFO  [io.quarkus] (Quarkus Main Thread) Profile dev activated. Live Coding activated.
2022-05-26 21:00:12,631 INFO  [io.quarkus] (Quarkus Main Thread) Installed features: [cdi, oidc, resteasy, security]
```

* Try to access to: http://localhost:8080. You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user.

#### Quarkus resource server application (backend)

The **service** type indicates that this application should authorize access based on bearer tokens.

* Change to app-5/quarkus/backend
* Change **CLIENT_SECRET** in **src/main/resources/application.properties** with secret of **backend-app** client
* Start the application:
```
λ mvnw quarkus:dev
[INFO] Scanning for projects...
[INFO]
[INFO] ------------------------< org.keycloak:quarkus >------------------------
[INFO] Building quarkus 1.0.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- quarkus-maven-plugin:1.11.3.Final:dev (default-cli) @ quarkus ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 2 resources
[INFO] Nothing to compile - all classes are up to date
Listening for transport dt_socket at address: 5005
__  ____  __  _____   ___  __ ____  ______
 --/ __ \/ / / / _ | / _ \/ //_/ / / / __/
 -/ /_/ / /_/ / __ |/ , _/ ,< / /_/ /\ \
--\___\_\____/_/ |_/_/|_/_/|_|\____/___/
2022-05-26 21:01:44,454 WARNING [io.ver.ext.aut.oau.imp.OAuth2AuthProviderImpl] (vert.x-eventloop-thread-2) Skipped unsupported JWK: java.security.NoSuchAlgorithmException: RSA-OAEP
2022-05-26 21:01:44,888 INFO  [io.quarkus] (Quarkus Main Thread) quarkus 1.0.0-SNAPSHOT on JVM (powered by Quarkus 1.11.3.Final) started in 3.662s. Listening on: http://localhost:8080
2022-05-26 21:01:44,897 INFO  [io.quarkus] (Quarkus Main Thread) Profile dev activated. Live Coding activated.
2022-05-26 21:01:44,904 INFO  [io.quarkus] (Quarkus Main Thread) Installed features: [cdi, oidc, resteasy, security]
```

* The application should be available at: http://localhost:8080
* To access the resource you will need an access token:
```
export access_token=$(curl -X POST "http://localhost:8180/realms/myrealm/protocol/openid-connect/token" -H "Content-Type:application/x-www-form-urlencoded" -d "grant_type=password" -d "client_id=backend-app" -d "client_secret=zk0ucogWXlGoLkUu9QLkN8nUjMQ3X6Uu" -d "username=user" -d "password=password" | jq --raw-output ".access_token")
```
* Call the **/hello** resource using the retrieved access_token:
```
curl -X GET "http://localhost:8080/hello" --header "Authorization: Bearer $access_token"
```
* You should get:
```
Hello RESTEasy
```

The **quarkus-oidc** extension validates tokens based on whether they
represent a JSON Web Token (JWT) or not. If the token is a JWT, the
extension will try to validate the token locally by checking its signatures, audience, and expiration date. Otherwise, if the token is opaque and the format is unknown, it will invoke the token's introspection endpoint at Keycloak to validate it.

### Using Spring Boot

Spring Boot applications can integrate with Keycloak by leveraging Spring Security's OAuth2/OpenID libraries.
There are two main libraries, where each is targeted by a specific type of application: clients and resource servers.

#### Spring Boot Client Application

* Change to app-5/springboot/frontend
* Change **CLIENT_SECRET** in the **src/main/resources/application.yaml** file with **server-side-app** client
* Start the application:
```
λ mvnw spring-boot:run
...
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.4.2)

2022-05-27 00:44:12.058  INFO 28584 --- [           main] com.example.springboot.Application       : Starting Application using Java 17.0.3 on DESKTOP-5LB04BU with PID 28584 
2022-05-27 00:44:12.061  INFO 28584 --- [           main] com.example.springboot.Application       : No active profile set, falling back to default profiles: default
2022-05-27 00:44:13.117  INFO 28584 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2022-05-27 00:44:13.127  INFO 28584 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-05-27 00:44:13.127  INFO 28584 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.41]
2022-05-27 00:44:13.196  INFO 28584 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-05-27 00:44:13.197  INFO 28584 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1048 ms
2022-05-27 00:44:13.426  INFO 28584 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
...
2022-05-27 00:44:13.752  INFO 28584 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2022-05-27 00:44:13.762  INFO 28584 --- [           main] com.example.springboot.Application       : Started Application in 2.222 seconds (JVM running for 2.619)
```

* Try to access to: http://localhost:8080. You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user and see greetings page.

#### Spring Boot resource server Application

In this case the application is going to act as a resource server that validates JWT tokens.

* Change to app-5/springboot/backend
* Start the application:
```
λ mvnw spring-boot:run
...
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.4.2)

2022-05-27 01:05:03.472  INFO 32512 --- [           main] org.keycloak.springboot.Application      : No active profile set, falling back to default profiles: default
2022-05-27 01:05:05.959  INFO 32512 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2022-05-27 01:05:05.985  INFO 32512 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2022-05-27 01:05:05.988  INFO 32512 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.41]
2022-05-27 01:05:06.138  INFO 32512 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2022-05-27 01:05:06.140  INFO 32512 --- [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 2493 ms
...
2022-05-27 01:05:07.067  INFO 32512 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2022-05-27 01:05:07.468  INFO 32512 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2022-05-27 01:05:07.495  INFO 32512 --- [           main] org.keycloak.springboot.Application      : Started Application in 5.342 seconds (JVM running for 6.208)
```

* The application should be available at: http://localhost:8080
* To access the resource you will need an access token:
```
export access_token=$(curl -X POST "http://localhost:8180/realms/myrealm/protocol/openid-connect/token" -H "Content-Type:application/x-www-form-urlencoded" -d "grant_type=password" -d "client_id=backend-app" -d "client_secret=zk0ucogWXlGoLkUu9QLkN8nUjMQ3X6Uu" -d "username=user" -d "password=password" | jq --raw-output ".access_token")
```
* Call the resource using the retrieved access_token:
```
curl -X GET "http://localhost:8080/" --header "Authorization: Bearer $access_token"
```
* You should get:
```
Greetings from Spring Boot!
```

## Integrating with JavaScript

You can integrate Keycloak with Single Page Applications (SPA) thanks to different OpenID Connect client implementations.

* Change to app-5/keycloak-js-adapter
* Start the application:
```
λ npm install
npm WARN deprecated start@5.1.0: Deprecated in favor of https://github.com/deepsweet/start
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN keycloak-js-app@0.0.1 No description
npm WARN keycloak-js-app@0.0.1 No repository field.
npm WARN keycloak-js-app@0.0.1 No license field.

added 61 packages from 47 contributors and audited 61 packages in 2.645s

8 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

λ npm start

> keycloak-js-app@0.0.1 start 
/app-5/keycloak-js-adapter
> node app.js
```
* Try to access to: http://localhost:8080. You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user and see greetings page.

## Integrating with Node.js

For Node.js Keycloak provides **Keycloak Node.js Adapter** that is targeted at integration with Keycloak rather than a generic OpenID Connect client
implementation. **Keycloak Node.js Adapter** hides most of the internals from your
application through a simple API that you can use to protect your
application resources.
The adapter is available as an npm package and can be installed into your project as follows:
```
λ npm install keycloak-connect
```

### Node.js Client application (frontend)

* Change to app-5/nodejs/frontend
* Change **CLIENT_SECRET** in the **keycloak.json** file with secret of **server-side-app** client
* Start the application:
```
λ npm install
> chromedriver@101.0.0 install app-5/nodejs/frontend/node_modules/chromedriver
> node install.js
ChromeDriver binary exists. Validating...
ChromeDriver is already available at '~/Temp/101.0.4951.41/chromedriver/chromedriver.exe'.
Copying from ~/Temp/101.0.4951.41/chromedriver to target path app-5/nodejs/frontend/node_modules/chromedriver/lib/chromedriver
Done. ChromeDriver binary available at app-5/nodejs/frontend/node_modules/chromedriver/lib/chromedriver/chromedriver.exe
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN keycloak-nodejs-frontend@0.0.1 No description
npm WARN keycloak-nodejs-frontend@0.0.1 No repository field.
npm WARN keycloak-nodejs-frontend@0.0.1 No license field.
added 147 packages from 137 contributors and audited 147 packages in 5.022s
17 packages are looking for funding
  run `npm fund` for details
found 0 vulnerabilities

λ npm start

> keycloak-nodejs-frontend@0.0.1 start app-5/nodejs/frontend
> node app.js

Started at port 8080
```
* Try to access to: http://localhost:8080. You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user and see greetings page.

### Node.js Resource Server application (backend)

* Change to app-5/nodejs/backend
* Change **CLIENT_SECRET** in the **keycloak.json** file with secret of **backend-app** client
* Start the application:
```
λ npm install

> chromedriver@101.0.0 install app-5/nodejs/backend/node_modules/chromedriver
> node install.js

ChromeDriver binary exists. Validating...
ChromeDriver is already available at '~/Temp/101.0.4951.41/chromedriver/chromedriver.exe'.
Copying from ~/Temp/101.0.4951.41/chromedriver to target path app-5/nodejs/backend/node_modules/chromedriver/lib/chromedriver
Done. ChromeDriver binary available at app-5/nodejs/backend/node_modules/chromedriver/lib/chromedriver/chromedriver.exe
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN keycloak-example-service@0.0.1 No description
npm WARN keycloak-example-service@0.0.1 No repository field.
npm WARN keycloak-example-service@0.0.1 No license field.

added 147 packages from 137 contributors and audited 147 packages in 5.343s

17 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

λ npm start

> keycloak-nodejs-frontend@0.0.1 start app-5/nodejs/backend
> node app.js

Started at port 8080
```

* The application should be available at: http://localhost:8080. if you try http://localhost:8080/protected you will get an: Access Denied response.
* To access the resource you will need an access token:
```
export access_token=$(curl -X POST "http://localhost:8180/realms/myrealm/protocol/openid-connect/token" -H "Content-Type:application/x-www-form-urlencoded" -d "grant_type=password" -d "client_id=backend-app" -d "client_secret=zk0ucogWXlGoLkUu9QLkN8nUjMQ3X6Uu" -d "username=user" -d "password=password" | jq --raw-output ".access_token")
```
* Call the resource using the retrieved access_token:
```
curl -X GET "http://localhost:8080/protected" --header "Authorization: Bearer $access_token"
```
* You should get:
```
Access granted to protected resource
```
## Integrating with Python

Python applications that use Flask can easily enable OpenID Connect and OAuth2 to applications through the **Flask-OIDC** library. It can be used to protect client as well as resource server applications.

### Flask Client application (frontend)

* Change to app-5/python/frontend
* Change **CLIENT_SECRET** in the **oidc-config.json** file with secret of server-side-app client
* Start the application:
```
λ flask run -p 8080
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:8080
```

* Try to access to: http://localhost:8080. You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user and see greetings page.

### Flask Resource Server application (backend)

* Change to app-5/nodejs/backend
* Change **CLIENT_SECRET** in the **oidc-config.json** file with secret of **backend-app** client
* Start the application:
```
λ  flask run -p 8080
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:8080
```
* The application should be available at: http://localhost:8080. if you try http://localhost:8080 you will get an: *{"error": "invalid_token", "error_description": "Token required but invalid"}*.
* To access the resource you will need an access token:
```
export access_token=$(curl -X POST "http://localhost:8180/realms/myrealm/protocol/openid-connect/token" -H "Content-Type:application/x-www-form-urlencoded" -d "grant_type=password" -d "client_id=backend-app" -d "client_secret=zk0ucogWXlGoLkUu9QLkN8nUjMQ3X6Uu" -d "username=user" -d "password=password" | jq --raw-output ".access_token")
```
* Call the resource using the retrieved access_token:
```
curl -X GET "http://localhost:8080" --header "Authorization: Bearer $access_token"
```
* You should get:
```
{"hello": "Welcome Hichem Kaighani"}
```

## Integrating With Keycloak Using a reverse proxy

A reverse proxy can be used to add additional capabilities to your application. The most common proxies provide support for OpenID Connect where enabling authentication is a matter of changing the proxy configuration.

* Change to app-5/reverse-proxy
* Start and set up your Apache HTTP Server thanks using **mod_auth_oidc**, the configuration to put in place is in **secure-proxy.conf** file
* Change **CLIENT_SECRET** in the **httpd.conf** file with secret of **proxy-client** client
* Start the application:
```
λ npm install
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN keycloak-example-app@0.0.1 No description
npm WARN keycloak-example-app@0.0.1 No repository field.
npm WARN keycloak-example-app@0.0.1 No license field.

added 57 packages from 42 contributors and audited 57 packages in 1.736s

7 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities

λ npm start

> keycloak-example-app@0.0.1 start 
\app-5\reverse-proxy\app
> node app.js
```
* The application is available on: http://localhost:8000
* The application is accessible through the reverse proxy on http://localhost, You should be redirected to Keycloak to authenticate
* Enter the user credentials, you should be redirected back to the application as authenticated user and see all requests headers displayed by the application:

```
Logout
Request headers
accept	text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
accept-encoding	gzip, deflate
accept-language	en-US,en;q=0.9,fr;q=0.8,ar;q=0.7
cache-control	max-age=0
connection	Keep-Alive
cookie	mod_auth_openidc_state_SICgFS7P5wtI5qilBl9V-MyRZcs=eyJhbGciOiAiZGlyIiwgImVuYyI6ICJBMjU2R0NNIn0..owpNRB4gKrCkoJ7C.-7GmiXT-9VsAAMEeuM-0PS-IPauzsmfUG4zIx2Cwb6i8kPXSelxbrtWYX5DhoEbOGwizOHTdubJ1Xa7nnLtSdVIRWTMuWx8_dI8JtMbUhfQvA2hyBPPZWyEjRgAFZxFFJuegdoBdC67JwrSW-9lQfJmqgORPyeRmQLetcpgYbePr787Lk3jvKor4UTwthQP9j1Te0AQWVvNZpAWkNcpCr6cSaNMW-aelP55GAYQl7Z_fA_QdvYlLSujwzGjR_4jtnmHfG1J8JJtzznZu8uAUze8tX0_AyAcEcnY9inlh8TkQNnBuVKL6p4ldGqDwjmpuZlxrCm9KrLSvaofluSapzG18hsTM4dZ7A3K1aEgRlVgitF7t8QGeTSdv.NOtoMX8lSpwR7t7okgojkA; mod_auth_openidc_state_U8sOvHdW9vAjrb0r6okF1iLZfkE=eyJhbGciOiAiZGlyIiwgImVuYyI6ICJBMjU2R0NNIn0..TVHor1k31sCFIunW.1VACAP69iDxCkXq2X_5vtVBN0QhOkzVrVZBKBgBWOicIZxWG2gouMnHHsszVHXtA-HeL6D8QqrQW1PeQRmvl4bi27DwphmaYcpb7gdOuwFiz5sSOwqJ0Pa7EzPlTLxE-A0ZTNuNGsxYTUe4tU1B6KfuGUTY-sGoyBEEVVRKKzgduEgwqj4sJjxBhJNjAhERqp8GnxWAXhT-lCQzyhnkHX2v5iOXV8I5xbK_Ob7yNxtneOG5KBJnvyQBsMIC2wqdQXh4PYEv28fT4MQiWnkMDDAzNG2RpW_s7WjklytMF7FkA2xPzo4N33QZ26ywk59VRVI8lJwpp4sVD1OlDMR-EvghnJOhmGMj0xouayIRo_z2aDQbCYaouSo8e.IDoqZ8FaXKjGh-sotAbE8w; mod_auth_openidc_session=623f1b56-2132-46f3-b651-0eddc8e09774
host	192.168.0.8:8000
oidc_access_token	eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJXWU1tTFRWalFULUNoUkNOR1lELWp3VS1fc1RreXZvM2pnUlB2eHdYTWM0In0.eyJleHAiOjE2NTM3Njc0OTUsImlhdCI6MTY1Mzc2NzE5NSwiYXV0aF90aW1lIjoxNjUzNzY3MTk1LCJqdGkiOiIyMDYxMWJiYS0zMDQwLTRkMjQtODNhYi1mNjIyOTQxZTVmMGEiLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4wLjg6ODE4MC9yZWFsbXMvbXlyZWFsbSIsImF1ZCI6ImFjY291bnQiLCJzdWIiOiJhOTVjNGM4MS1mZDIxLTRhNWUtOWJlNS03ZTBkMWZhZjhlYzMiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJwcm94eS1jbGllbnQiLCJub25jZSI6IkxVRkFXVS1jLWxyT1RyZTIzX18wVlpVbTY4N0FJaUJYa3l3NXExbndhMmsiLCJzZXNzaW9uX3N0YXRlIjoiN2ViNWQwZjgtY2Y3ZS00YjY4LTgxNmMtN2UwYTdiOWMwOTJkIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyJodHRwOi8vbG9jYWxob3N0Il0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJkZWZhdWx0LXJvbGVzLW15cmVhbG0iLCJvZmZsaW5lX2FjY2VzcyIsInNlY3VyZWRFbmRwb2ludCIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJvcGVuaWQgcHJvZmlsZSBlbWFpbCIsInNpZCI6IjdlYjVkMGY4LWNmN2UtNGI2OC04MTZjLTdlMGE3YjljMDkyZCIsImVtYWlsX3ZlcmlmaWVkIjpmYWxzZSwibmFtZSI6IkhpY2hlbSBLYWlnaGFuaSIsInByZWZlcnJlZF91c2VybmFtZSI6ImhrYWlnaGFuaSIsImdpdmVuX25hbWUiOiJIaWNoZW0iLCJmYW1pbHlfbmFtZSI6IkthaWdoYW5pIiwicGljdHVyZSI6Imh0dHA6Ly9kYXRhc2Vuc2UtZ3JvdXAuZnI6ODgvd3AtY29udGVudC91cGxvYWRzLzIwMjIvMDIvbG9nb19kYXRhXzA2LnBuZyIsImVtYWlsIjoiaGljaGVtLmthaWdoYW5pQGRhdGFzZW5zZS1ncm91cC5jb20ifQ.TWJrsGIVeKW5mofBUWVUs1R6SnJyeZeKcLxJnCdU1Et_XzCv1DiIminFvaNrybHdhU9NhYcytvCK7Dgr-EYp6L6MPLyP7fiQNm_aQyhrNb1jM6kRshWoQ-C0QaGMT3H6n156VO4Ao2E-SbhiKEBbslN8_Obo-P8EPJzuwJJkzEbAgLYAfrR1-oCA_CGl_4yPh-6YzC0Qbk0GClaU5Q8Y4dd3_wDqxllEY2o2RYhPysOCKJp6s8oZr3nxzTO1GMBxX4C0F8k69MOtDrg8dj0ukcTe-IKnHTYPrqQ6PGRO2Hn1UKju6QvnmY1cGd6TXVeErH8uCzH5kScJiKww16btPw
oidc_access_token_expires	1653767495
oidc_claim_acr	1
oidc_claim_at_hash	j1j0qLmuqFzGqBEmM9sPqA
oidc_claim_aud	proxy-client
oidc_claim_auth_time	1653767195
oidc_claim_azp	proxy-client
oidc_claim_email	hichem.kaighani@datasense-group.com
oidc_claim_email_verified	0
oidc_claim_exp	1653767495
oidc_claim_family_name	Kaighani
oidc_claim_given_name	Hichem
oidc_claim_iat	1653767195
oidc_claim_iss	http://192.168.0.8:8180/realms/myrealm
oidc_claim_jti	79063569-4be3-4bad-b65f-f94a2e8962dd
oidc_claim_name	Hichem Kaighani
oidc_claim_nonce	LUFAWU-c-lrOTre23__0VZUm687AIiBXkyw5q1nwa2k
oidc_claim_picture	http://datasense-group.fr:88/wp-content/uploads/2022/02/logo_data_06.png
oidc_claim_preferred_username	hkaighani
oidc_claim_realm_access	{"roles": ["default-roles-myrealm", "offline_access", "securedEndpoint", "uma_authorization"]}
oidc_claim_session_state	7eb5d0f8-cf7e-4b68-816c-7e0a7b9c092d
oidc_claim_sid	7eb5d0f8-cf7e-4b68-816c-7e0a7b9c092d
oidc_claim_sub	a95c4c81-fd21-4a5e-9be5-7e0d1faf8ec3
oidc_claim_typ	ID
upgrade-insecure-requests	1
user-agent	Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.67 Safari/537.36
x-forwarded-for	172.17.0.1
x-forwarded-host	192.168.0.8
x-forwarded-server	localhost
```